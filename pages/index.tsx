import Head from 'next/head'
import {env} from '../unsafeDotENV'

export default function Home(props: any) {
  console.log('PROPS', props)

  return (
    <>
      <Head>
        <title>DotCMS - Next.js & Tailwind CSS</title>
        <meta name="description" content="DotCMS - Next.js & Tailwind CSS" />
        <link rel="icon" href="/favicon.ico" />
        <p> Pulling {props.page.friendlyName} from {props.page.hostName}</p>
      </Head>

      <h1>DotCMS - Next.js & Tailwind CSS</h1>
    </>
  )
}

export async function getServerSideProps(context: any) {
  const { containers, layout, page } = await fetch("http://34.222.167.67:8080/api/v1/page/render/test-from-host?language_id=1",
    {
      headers: {
        Authorization: `Bearer ${env.UNSAFE_AUTH_TOKEN}`,
      },
    }
  )
  .then((res) => res.json())
  .then((data) => data.entity);

  return {
    props: { containers, layout, page }, // will be passed to the page component as props
  };
}